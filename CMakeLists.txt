cmake_minimum_required(VERSION 3.12)
project(sik_netstore2)

set(CMAKE_CXX_STANDARD 17)

add_definitions(-DBOOST_LOG_DYN_LINK -DDEBUG)

find_package(Boost COMPONENTS program_options log filesystem REQUIRED)
include_directories(${BOOST_INCLUDE_DIRS})
set(Boost_USE_STATIC_LIBS ON)

add_library(netstore-common OBJECT src/common/types.hpp src/common/udp_socket.hpp src/common/tcp_server_socket.hpp src/common/address.hpp src/common/exceptions/socket_exception.hpp src/common/utils.hpp src/common/tcp_connection.hpp src/common/tcp_client_socket.hpp src/common/file_transmitter.hpp)
add_executable(netstore-client src/client/client_main.cpp $<TARGET_OBJECTS:netstore-common> src/client/client.hpp)
add_executable(netstore-server src/server/server_main.cpp src/server/server.hpp $<TARGET_OBJECTS:netstore-common> src/server/files.hpp)

target_link_libraries(netstore-client ${Boost_LIBRARIES} -lboost_log_setup  -lboost_log -lpthread -lm)
target_link_libraries(netstore-server ${Boost_LIBRARIES} -lboost_log_setup  -lboost_log -lpthread -lm)

