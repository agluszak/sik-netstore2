CC = g++
LFLAGS= -Wall -Wextra -O2 -std=c++17 -lboost_log_setup -lboost_system -lboost_program_options -lboost_log -lpthread -lm -lboost_filesystem -I /usr/include/boost -DBOOST_LOG_DYN_LINK
# proszę dodać -DDEBUG dla lepszych komunikatów

all: netstore-server netstore-client

netstore-server: src/server/server_main.cpp
	$(CC) $(LFLAGS) src/server/server_main.cpp -o netstore-server

netstore-client: src/client/client_main.cpp
	$(CC) $(LFLAGS) src/client/client_main.cpp -o netstore-client

.PHONY: clean

clean:
	rm -f netstore-server netstore-client *.o *~ *.bak
