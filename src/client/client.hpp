#ifndef SIK_NETSTORE2_CLIENT_HPP
#define SIK_NETSTORE2_CLIENT_HPP

#include <iostream>
#include <vector>
#include "../common/udp_socket.hpp"
#include "../common/types.hpp"
#include "../common/utils.hpp"
#include "../common/tcp_server_socket.hpp"
#include "../common/tcp_client_socket.hpp"
#include <boost/log/trivial.hpp>
#include <chrono>
#include <boost/algorithm/string.hpp>
#include <regex>
#include <fstream>
#include <boost/filesystem/operations.hpp>
#include <thread>
#include <atomic>

namespace netstore2 {
    namespace client {

        using namespace std;

        class Client {
            string group_address;
            unsigned short server_port;
            boost::filesystem::path path;
            uint64_t timeout;
            UdpSocket socket;

            struct DiscoveredServer {
                string multicast_address;
                string unicast_address;
                uint64_t free_space;
            };

            struct DiscoveredFile {
                string unicast_address;
                string filename;
            };

            vector<DiscoveredServer> discovered_servers;
            vector<DiscoveredFile> discovered_files;

        public:
            Client(const string& group_address, unsigned short port, const string& path, uint64_t timeout) :
                    group_address(group_address), server_port(port), path(path),
                    timeout(timeout) {
                socket.set_broadcast(true);
                socket.set_send_timeout(chrono::seconds(timeout));

                if (!boost::filesystem::exists(path)) {
                    throw runtime_error("Directory " + path + " does not exist");
                }
            }

            template<typename NameCheck, typename Action, typename TimeoutAction>
            bool receive_command(NameCheck name_check, uint64_t expected_cmd_seq, Action action, TimeoutAction timeoutAction) {
                Address address;
                size_t received_length = 0;
                optional<Command> response = socket.receive_command(address, received_length);
                if (response) {
                    log_debug(boost::format("%1% %2%") % address.to_str() % response->to_str());
                    if (!name_check(response->name)) {
                        log_error(
                                boost::format("[PCKG ERROR] Skipping invalid package from %1%. Wrong command - %2%") %
                                address.to_str() % to_string(response->name));
                    } else if (response->cmd_seq != expected_cmd_seq) {
                        log_error(boost::format(
                                "[PCKG ERROR] Skipping invalid package from %1%. Cmd_seq was %2% instead of %3%") %
                                  address.to_str() % response->cmd_seq % expected_cmd_seq);
                    } else {
                        action(*response, address);
                        return true;
                    }
                } else {
                    if (received_length > 0) {
                        log_error(boost::format("[PCKG ERROR] Skipping invalid package from %1%. Read %2% bytes") %
                                  address.to_str() % received_length);
                    } else {
                        timeoutAction();
                    }
                }
                return false;
            }

            template<typename Action>
            void listen_for_given_time(CommandName expected_name, uint64_t expected_cmd_seq, Action action) {
                listen_for_given_time([expected_name](CommandName& name) {
                    return name == expected_name;
                }, expected_cmd_seq, action, false);
            }

            template<typename NameCheck, typename Action>
            void listen_for_given_time(NameCheck name_check, uint64_t expected_cmd_seq, Action action,
                                       bool stop_after_first_success) {
                auto deadline = chrono::system_clock::now() + chrono::seconds(timeout);
                auto now = chrono::system_clock::now();
                while (now < deadline) {
                    auto time_left = deadline - now;
                    socket.set_receive_timeout(time_left);
                    bool success = receive_command(name_check, expected_cmd_seq, action, [](){});
                    if (success && stop_after_first_success) {
                        break;
                    }
                    now = chrono::system_clock::now();
                }
            }

            inline uint64_t generate_timestamp() {
                return chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()
                                                                           .time_since_epoch()).count();
            }

            void handle_discover() {
                discovered_servers.clear();
                Address dest(group_address, server_port);
                log_debug(boost::format("Discovering started"));
                uint64_t timestamp = generate_timestamp();
                Command hello(CommandName::HELLO, timestamp, "");
                socket.send_command(hello, dest);

                listen_for_given_time(CommandName::GOOD_DAY, timestamp, [this](Command& response, Address& address) {
                    log(boost::format("Found %1% (%2%) with free space %3%") % address.get_address_dot() %
                        response.data % response.param);

                    DiscoveredServer server = {.multicast_address = response.data,
                            .unicast_address = address.get_address_dot(),
                            .free_space = response.param};
                    discovered_servers.push_back(server);
                });

                log_debug(boost::format("Discovering stopped"));
            }

            void handle_search(string name) {
                discovered_files.clear();
                Address dest(group_address, server_port);
                log_debug(boost::format("Searching started"));
                uint64_t timestamp = generate_timestamp();
                Command list(CommandName::LIST, timestamp, name);
                socket.send_command(list, dest);
                listen_for_given_time(CommandName::MY_LIST, timestamp, [this](Command& response, Address& address) {
                    vector<string> filenames;
                    boost::split(filenames, response.data, boost::is_any_of("\n"));

                    for (string& filename : filenames) {
                        log(boost::format("%1% (%2%)") % filename % address.get_address_dot());
                        DiscoveredFile file{.unicast_address = address.get_address_dot(), .filename=filename};
                        discovered_files.push_back(file);
                    }

                });
                log_debug(boost::format("Searching stopped"));
            }

            void handle_fetch(string name) {
                auto file_found_iterator = find_if(discovered_files.begin(), discovered_files.end(),
                                                   [name](DiscoveredFile& file) {
                                                       return file.filename == name;
                                                   });

                if (file_found_iterator != discovered_files.end()) {
                    Address dest(file_found_iterator->unicast_address, server_port);
                    uint64_t timestamp = generate_timestamp();
                    Command get(CommandName::GET, timestamp, name);
                    socket.send_command(get, dest);
                    socket.set_receive_timeout(chrono::seconds(timeout));

                    receive_command([](CommandName& name) { return name == CommandName::CONNECT_ME; }, timestamp,
                                    [this](Command& response, Address& address) {
                                        string file_path = (path / response.data).string();
                                        log_debug(boost::format("File will be saved to %1%") % file_path);
                                        thread thread([file_path, response, address]() {
                                            try {
                                                ofstream file(file_path, std::ofstream::binary);

                                                TcpClientSocket tcp_socket(address.get_address_dot(), response.param);
                                                log_debug(boost::format("TCP connected (%1%:%2%)") %
                                                          address.get_address_dot() %
                                                          response.param);
                                                tcp_socket.receive_file(file);
                                                log(boost::format("File %1% downloaded (%2%:%3%)") % response.data %
                                                    address.get_address_dot() % response.param);
                                            } catch (exception& e) {
                                                log(boost::format("File %1% downloading failed (%2%:%3%). %4%") %
                                                    response.data %
                                                    address.get_address_dot() % response.param % e.what());
                                            }

                                        });
                                        thread.detach();
                                    }, [name, dest](){
                                log(boost::format(
                                        "File %1% downloading failed (%2%:). Server did not respond") %
                                    name % dest.get_address_dot());

                    });
                } else {
                    log(boost::format(
                            "File %1% downloading failed (:). No such file was found on the list of recently searched") %
                        name);
                }
            }

            void handle_upload(string upload_path_str) {
                boost::filesystem::path upload_path(upload_path_str);
                if (boost::filesystem::exists(upload_path) && boost::filesystem::is_regular_file(upload_path)) {
                    size_t file_size = boost::filesystem::file_size(upload_path);
                    vector<DiscoveredServer> servers_sorted(discovered_servers);
                    servers_sorted.erase(remove_if(servers_sorted.begin(),
                                                   servers_sorted.end(),
                                                   [file_size](DiscoveredServer& server) {
                                                       return server.free_space < file_size;
                                                   }),
                                         servers_sorted.end());
                    sort(servers_sorted.begin(), servers_sorted.end(),
                         [](DiscoveredServer& first, DiscoveredServer& second) {
                             return first.free_space > second.free_space;
                         });

                    bool upload_success = false;
                    for (size_t i = 0; i < servers_sorted.size(); i++) {
                        uint64_t timestamp = generate_timestamp();
                        Command add(CommandName::ADD, timestamp, file_size, upload_path.filename().string());
                        Address server_address(servers_sorted[i].unicast_address, server_port);
                        socket.send_command(add, server_address);

                        listen_for_given_time([](CommandName& name) {
                            return name == CommandName::NO_WAY || name == CommandName::CAN_ADD;
                        }, timestamp, [this, upload_path_str, &upload_success](Command& response, Address& address) {
                            if (response.name == CommandName::NO_WAY) {
                                log_debug(
                                        boost::format("File %1% uploading failed (%2%:). Server responded NO WAY") %
                                        upload_path_str %
                                        address.get_address_dot());
                            } else {
                                if (response.data.length() != 0) {
                                    log_error(boost::format(
                                            "[PCKG ERROR] Skipping invalid package from %1%. CAN_ADD packet had non-empty data field") %
                                              address.to_str());
                                } else {
                                    upload_success = true;
                                    thread thread([&upload_success, upload_path_str, response, address]() {
                                        try {
                                            ifstream file(upload_path_str, std::ifstream::binary);

                                            TcpClientSocket tcp_socket(address.get_address_dot(), response.param);
                                            log_debug(boost::format("TCP connected (%1%:%2%)") %
                                                      address.get_address_dot() %
                                                      response.param);
                                            tcp_socket.send_file(file);
                                            log(boost::format("File %1% uploaded (%2%:%3%)") % upload_path_str %
                                                address.get_address_dot() % response.param);
                                        } catch (exception& e) {
                                            log(boost::format("File %1% uploading failed (%2%:%3%). %4%") %
                                                upload_path_str %
                                                address.get_address_dot() % response.param % e.what());
                                        }
                                    });
                                    thread.detach();
                                }
                            }

                        }, true);
                        if (upload_success) {
                            break;
                        }
                    }
                    if (!upload_success) {
                        log(boost::format(
                                "File %1% too big") %
                            upload_path_str);
                    }
                } else {
                    log(boost::format(
                            "File %1% does not exist") %
                        upload_path_str);
                }
            }

            void handle_delete(string file_name) {
                Address dest(group_address, server_port);
                uint64_t timestamp = generate_timestamp();
                Command del(CommandName::DEL, timestamp, file_name);
                socket.send_command(del, dest);
                log_debug(boost::format("Requested deletion of file %1%") % file_name);
            }

            void start() {
                log_debug(boost::format("Client started"));

                regex discover_regex("discover");
                regex search_regex("search (.+)");
                regex search_no_name_regex("search ?");
                regex fetch_regex("fetch (.+)");
                regex upload_regex("upload (.+)");
                regex remove_regex("remove (.+)");
                regex exit_regex("exit");

                smatch match;

                string requested_command;
                while (true) {
                    getline(cin, requested_command);
                    if (regex_match(requested_command, discover_regex)) {
                        handle_discover();
                    } else if (regex_match(requested_command, match, search_regex)) {
                        handle_search(match[1].str());
                    } else if (regex_match(requested_command, search_no_name_regex)) {
                        handle_search("");
                    } else if (regex_match(requested_command, match, fetch_regex)) {
                        handle_fetch(match[1].str());
                    } else if (regex_match(requested_command, match, upload_regex)) {
                        handle_upload(match[1].str());
                    } else if (regex_match(requested_command, match, remove_regex)) {
                        handle_delete(match[1].str());
                    } else if (regex_match(requested_command, exit_regex)) {
                        break;
                    } else {
                        log_debug(boost::format("Unrecognized command"));
                    }
                }
                log_debug(boost::format("Shutting down..."));
            }
        };
    }
}

#endif //SIK_NETSTORE2_CLIENT_HPP
