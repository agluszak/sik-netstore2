#ifndef SIK_NETSTORE2_ADDRESS_HPP
#define SIK_NETSTORE2_ADDRESS_HPP

#include <netinet/in.h>
#include <string>
#include <arpa/inet.h>
#include "exceptions/socket_exception.hpp"

namespace netstore2 {

    using namespace std;

    class Address {
        sockaddr_in addr;
        string address_dot;
        unsigned short port;
        string str;

    public:
        Address() {
            str = "NOT SET";
        }

        Address(sockaddr_in& addr) : addr(addr) {
            address_dot = inet_ntoa(addr.sin_addr);
            port = ntohs(addr.sin_port);
            str = address_dot + ":" + to_string(port);
        }

        Address(string& address, unsigned short port) : port(port) {
            addrinfo* addr_result;

            if (getaddrinfo(address.c_str(), nullptr, nullptr, &addr_result) != 0) {
                throw SocketException("getaddrinfo " + address, true);
            }

            addr.sin_family = AF_INET;
            addr.sin_addr.s_addr =
                    ((struct sockaddr_in*) (addr_result->ai_addr))->sin_addr.s_addr;
            addr.sin_port = htons(port);

            freeaddrinfo(addr_result);

            address_dot = inet_ntoa(addr.sin_addr);
            str = address_dot + ":" + to_string(port);
        }

        sockaddr_in get_addr() const {
            return addr;
        }

        string get_address_dot() const {
            return address_dot;
        }

        string to_str() const {
            return str;
        }

        unsigned short get_port() const {
            return port;
        }
    };
}

#endif //SIK_NETSTORE2_ADDRESS_HPP
