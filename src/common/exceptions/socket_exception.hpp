#ifndef SIK_NETSTORE2_SOCKET_EXCEPTION_HPP
#define SIK_NETSTORE2_SOCKET_EXCEPTION_HPP

#include <exception>
#include <string>
#include <cstring>

namespace netstore2 {
    using namespace std;

    class SocketException : public exception {
        string message;

    public:
        SocketException(const string& user_message, bool include_errno) : message(user_message) {
            if (include_errno) {
                message.append(": ");
                message.append(strerror(errno));
            }
        }

        const char* what() const noexcept override {
            return message.c_str();
        }
    };
}

#endif //SIK_NETSTORE2_SOCKET_EXCEPTION_HPP
