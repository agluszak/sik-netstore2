#ifndef SIK_NETSTORE2_FILE_TRANSMITTER_HPP
#define SIK_NETSTORE2_FILE_TRANSMITTER_HPP

#include <fstream>
#include "exceptions/socket_exception.hpp"
#include <cstdio>
#include <unistd.h>

namespace netstore2 {
    using namespace std;

    static constexpr size_t tcp_buffer_size = 256 * 256;

    void receive_file(ofstream& file, int fd) {
        char buffer[tcp_buffer_size];

        ssize_t read_now = 0;

        do {
            read_now = read(fd, buffer, tcp_buffer_size);
            file.write(buffer, read_now);
            if (read_now < 0) {
                throw SocketException("read", true);
            }
        } while (read_now > 0);
    }

    void receive_file(ofstream& file, int fd, uint64_t max) {
        char buffer[tcp_buffer_size];

        ssize_t read_now = 0;
        size_t total_received = 0;

        do {
            read_now = read(fd, buffer, tcp_buffer_size);
            if (read_now < 0) {
                throw SocketException("read", true);
            }

            if (total_received + read_now > max) {
                size_t left_to_write = max - total_received;
                file.write(buffer, left_to_write);
                total_received += left_to_write;
                log_debug(boost::format("Skipping additional %1% bytes") % (read_now - left_to_write));
            } else {
                file.write(buffer, read_now);
                total_received += read_now;
            }

        } while (read_now > 0);


    }

    void send_file(ifstream& file, int fd) {
        char buffer[tcp_buffer_size];

        ssize_t read_now = 0;
        ssize_t written_now = 0;

        do {
            read_now = file.read(buffer, tcp_buffer_size).gcount();
            written_now = write(fd, buffer, read_now);
        } while (written_now > 0);

        if (written_now < 0) {
            throw SocketException("write", true);
        }

    }
};

#endif //SIK_NETSTORE2_FILE_TRANSMITTER_HPP
