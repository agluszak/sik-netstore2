#ifndef SIK_NETSTORE2_TCP_CLIENT_SOCKET_HPP
#define SIK_NETSTORE2_TCP_CLIENT_SOCKET_HPP

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <sys/select.h>
#include "address.hpp"
#include "types.hpp"
#include "utils.hpp"
#include <unistd.h>
#include <fcntl.h>
#include "exceptions/socket_exception.hpp"
#include "tcp_connection.hpp"
#include <chrono>
#include <mutex>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fstream>
#include <netdb.h>

namespace netstore2 {
    using namespace std;

    class TcpClientSocket {
        int fd = -1;
        unsigned short port;

    public:

        TcpClientSocket(string server_address, unsigned short port_num) : port(port_num) {
            struct addrinfo addr_hints{};
            struct addrinfo* addr_result;

            memset(&addr_hints, 0, sizeof(struct addrinfo));
            addr_hints.ai_family = AF_INET;
            addr_hints.ai_socktype = SOCK_STREAM;
            addr_hints.ai_protocol = IPPROTO_TCP;

            int err = getaddrinfo(server_address.c_str(), std::to_string(port_num).c_str(), &addr_hints, &addr_result);
            if (err == EAI_SYSTEM) {
                throw SocketException((boost::format("getaddrinfo: %1%") % gai_strerror(err)).str(), true);
            } else if (err != 0) {
                throw SocketException((boost::format("getaddrinfo: %1%") % gai_strerror(err)).str(), true);
            }

            fd = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
            if (fd < 0) {
                throw SocketException("socket", true);
            }

            if (::connect(fd, addr_result->ai_addr, addr_result->ai_addrlen) < 0) {
                close(fd);
                throw SocketException("connect", true);
            }

            freeaddrinfo(addr_result);
        }

        ~TcpClientSocket() {
            close(fd);
        }

        unsigned short get_port() {
            return port;
        }

        void receive_file(ofstream& file) {
            ::netstore2::receive_file(file, fd);
        }

        void send_file(ifstream& file) {
            ::netstore2::send_file(file, fd);
        }
    };
}

#endif //SIK_NETSTORE2_TCP_CLIENT_SOCKET_HPP
