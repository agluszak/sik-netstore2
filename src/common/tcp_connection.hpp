#ifndef SIK_NETSTORE2_TCP_CONNECTION_HPP
#define SIK_NETSTORE2_TCP_CONNECTION_HPP

#include "file_transmitter.hpp"

namespace netstore2 {
    using namespace std;

    class TcpConnection {
        int fd;

    public:
        TcpConnection(int fd) : fd(fd) {
        }

        ~TcpConnection() {
            close(fd);
        }

        void receive_file(ofstream& file, uint64_t max) {
            ::netstore2::receive_file(file, fd, max);
        }

        void send_file(ifstream& file) {
            ::netstore2::send_file(file, fd);
        }

    };
}

#endif //SIK_NETSTORE2_TCP_CONNECTION_HPP
