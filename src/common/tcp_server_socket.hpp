#ifndef SIK_NETSTORE2_TCP_SERVER_SOCKET_HPP
#define SIK_NETSTORE2_TCP_SERVER_SOCKET_HPP

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <sys/select.h>
#include "address.hpp"
#include "types.hpp"
#include "utils.hpp"
#include <unistd.h>
#include <fcntl.h>
#include "exceptions/socket_exception.hpp"
#include "tcp_connection.hpp"
#include <chrono>
#include <mutex>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fstream>
#include <netdb.h>

namespace netstore2 {
    using namespace std;

    class TcpServerSocket {
        int fd = -1;
        sockaddr_in address{};
        unsigned short port;

    public:
        TcpServerSocket() {
            fd = socket(PF_INET, SOCK_STREAM, 0);
            if (fd < 0) {
                throw SocketException("socket", true);
            }
            address.sin_family = AF_INET;
            address.sin_addr.s_addr = htonl(INADDR_ANY);
            address.sin_port = 0;

            if (bind(fd, (struct sockaddr*) &address,
                     (socklen_t) sizeof(address)) < 0) {
                close(fd);
                throw SocketException("bind", true);
            }

            socklen_t len = (socklen_t) sizeof(address);
            if (getsockname(fd, (struct sockaddr*) &address, &len) < 0) {
                close(fd);
                throw SocketException("getsockname", true);
            }
            port = ntohs(address.sin_port);

            if (listen(fd, 1) < 0) {
                close(fd);
                throw SocketException("listen", true);
            }
        }

        TcpServerSocket(TcpServerSocket&& other) {
            fd = other.fd;
            port = other.port;
            address = other.address;
            other.fd = -1;
        }

        ~TcpServerSocket() {
            if (fd >= 0) {
                close(fd);
            }
        }

        unsigned short get_port() const {
            return port;
        }

        TcpConnection accept(uint64_t timeout) const {
            fd_set fdset;
            timeval tv{};

            fcntl(fd, F_SETFL, O_NONBLOCK);
            FD_ZERO(&fdset);
            FD_SET(fd, &fdset);
            tv.tv_sec = timeout;
            tv.tv_usec = 0;
            sockaddr_in client_address{};
            socklen_t client_address_len = sizeof(client_address);

            if (select(fd + 1, &fdset, nullptr, nullptr, &tv) == 1) {
                int connection_fd = ::accept(fd, (struct sockaddr*) &client_address, &client_address_len);
                if (connection_fd < 0 && errno != EAGAIN) {
                    throw SocketException("accept", true);
                }
                return TcpConnection(connection_fd);
            }
            throw SocketException("timeout", false);
        }

    };
}


#endif //SIK_NETSTORE2_TCP_SERVER_SOCKET_HPP
