#ifndef SIK_NETSTORE2_TYPES_HPP
#define SIK_NETSTORE2_TYPES_HPP

#include <string>
#include <cstdint>
#include <optional>
#include <cstring>

namespace netstore2 {
    using namespace std;

    enum class CommandName {
        HELLO, LIST, MY_LIST, GET, DEL, NO_WAY, GOOD_DAY, CONNECT_ME, ADD, CAN_ADD
    };

    char hello_str[10] = {'H', 'E', 'L', 'L', 'O', '\0', '\0', '\0', '\0', '\0'};
    char list_str[10] = {'L', 'I', 'S', 'T', '\0', '\0', '\0', '\0', '\0', '\0'};
    char my_list_str[10] = {'M', 'Y', '_', 'L', 'I', 'S', 'T', '\0', '\0', '\0'};
    char get_str[10] = {'G', 'E', 'T', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
    char del_str[10] = {'D', 'E', 'L', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
    char no_way_str[10] = {'N', 'O', '_', 'W', 'A', 'Y', '\0', '\0', '\0', '\0'};
    char good_day_str[10] = {'G', 'O', 'O', 'D', '_', 'D', 'A', 'Y', '\0', '\0'};
    char connect_me_str[10] = {'C', 'O', 'N', 'N', 'E', 'C', 'T', '_', 'M', 'E'};
    char add_str[10] = {'A', 'D', 'D', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
    char can_add_str[10] = {'C', 'A', 'N', '_', 'A', 'D', 'D', '\0', '\0', '\0'};

    optional<CommandName> parse_command(char* s) {
        // Simple
        if (memcmp(s, hello_str, 10) == 0) {
            return CommandName::HELLO;
        } else if (memcmp(s, list_str, 10) == 0) {
            return CommandName::LIST;
        } else if (memcmp(s, my_list_str, 10) == 0) {
            return CommandName::MY_LIST;
        } else if (memcmp(s, get_str, 10) == 0) {
            return CommandName::GET;
        } else if (memcmp(s, del_str, 10) == 0) {
            return CommandName::DEL;
        } else if (memcmp(s, no_way_str, 10) == 0) {
            return CommandName::NO_WAY;
            // Complex
        } else if (memcmp(s, add_str, 10) == 0) {
            return CommandName::ADD;
        } else if (memcmp(s, can_add_str, 10) == 0) {
            return CommandName::CAN_ADD;
        } else if (memcmp(s, connect_me_str, 10) == 0) {
            return CommandName::CONNECT_ME;
        } else if (memcmp(s, good_day_str, 10) == 0) {
            return CommandName::GOOD_DAY;
        } else {
            return nullopt;
        }
    }

    bool is_simple_command(CommandName name) {
        switch (name) {
            case CommandName::GOOD_DAY:
            case CommandName::CONNECT_ME:
            case CommandName::ADD:
            case CommandName::CAN_ADD:
                return false;
            default:
                return true;
        }
    }

    string to_string(CommandName name) {
        switch (name) {
            // Simple
            case CommandName::HELLO:
                return "HELLO";
            case CommandName::LIST:
                return "LIST";
            case CommandName::MY_LIST:
                return "MY_LIST";
            case CommandName::GET:
                return "GET";
            case CommandName::DEL:
                return "DEL";
            case CommandName::NO_WAY:
                return "NO_WAY";
                // Complex
            case CommandName::GOOD_DAY:
                return "GOOD_DAY";
            case CommandName::CONNECT_ME:
                return "CONNECT_ME";
            case CommandName::ADD:
                return "ADD";
            case CommandName::CAN_ADD:
                return "CAN_ADD";
        }
        assert(false);
    }

    void to_c_string_of_length_10(char buff[10], string str) {
        for (size_t i = 0; i < str.length(); i++) {
            buff[i] = str[i];
        }
        for (size_t i = str.length(); i < 10; i++) {
            buff[i] = '\0';
        }
    }

    struct Command {
        CommandName name;
        uint64_t cmd_seq;
        string data;
        uint64_t param;

        Command(CommandName name, uint64_t cmd_seq, string data) :
                name(name), cmd_seq(cmd_seq), data(data), param(0) {}

        Command(CommandName name, uint64_t cmd_seq, uint64_t param, string data) :
                name(name), cmd_seq(cmd_seq), data(data), param(param) {}

        static optional<Command> from_bytes(char* bytes, size_t length) {
            if (length < 10 + sizeof(uint64_t)) {
                return nullopt;
            }
            optional<CommandName> name = parse_command(bytes);
            if (!name) {
                return nullopt;
            }
            uint64_t cmd_seq;
            memcpy(&cmd_seq, bytes + 10, sizeof(uint64_t));
            if (is_simple_command(*name)) {
                string data(bytes + 10 + sizeof(uint64_t), length - 10 - sizeof(uint64_t));
                return Command(*name, be64toh(cmd_seq), data);
            } else {
                uint64_t param;
                memcpy(&param, bytes + 10 + sizeof(uint64_t), sizeof(uint64_t));
                string data(bytes + 10 + sizeof(uint64_t) + sizeof(uint64_t),
                            length - 10 - sizeof(uint64_t) - sizeof(uint64_t));
                return Command(*name, be64toh(cmd_seq), be64toh(param), data);
            }
        }

        size_t to_bytes(char* buffer) {
            to_c_string_of_length_10(buffer, to_string(name));
            uint64_t _cmd_seq = htobe64(cmd_seq);
            memcpy(buffer + 10, &_cmd_seq, sizeof(uint64_t));
            if (is_simple_command(name)) {
                size_t data_length = data.length();
                memcpy(buffer + 10 + sizeof(uint64_t), data.c_str(), data_length);
                return 10 + sizeof(uint64_t) + data_length;
            } else {
                uint64_t _param = htobe64(param);
                memcpy(buffer + 10 + sizeof(uint64_t), &_param, sizeof(uint64_t));
                size_t data_length = data.length();
                memcpy(buffer + 10 + sizeof(uint64_t) + sizeof(uint64_t), data.c_str(), data_length);
                return 10 + sizeof(uint64_t) + sizeof(uint64_t) + data_length;
            }
        }

        string to_str() {
            if (is_simple_command(this->name)) {
                return to_string(name) + " " + std::to_string(cmd_seq) + " " + data;
            } else {
                return to_string(name) + " " + std::to_string(cmd_seq) + " " + std::to_string(param) + " " + data;
            }
        }
    };
}
#endif //SIK_NETSTORE2_TYPES_HPP
