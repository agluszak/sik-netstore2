#ifndef SIK_NETSTORE2_UDP_SOCKET_HPP
#define SIK_NETSTORE2_UDP_SOCKET_HPP

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdio>
#include <unistd.h>
#include <string>
#include "address.hpp"
#include "types.hpp"
#include "utils.hpp"
#include "exceptions/socket_exception.hpp"
#include <chrono>
#include <mutex>

namespace netstore2 {
    using namespace std;

    static constexpr size_t UDP_PACKET_SIZE = 65535 + 1;

    class UdpSocket {
        int fd;
        sockaddr_in address;
        char buffer[UDP_PACKET_SIZE];

    public:
        UdpSocket() {
            fd = socket(PF_INET, SOCK_DGRAM, 0);
            if (fd < 0) {
                throw SocketException("socket", true);
            }
        }

        UdpSocket(int port_num) {
            fd = socket(AF_INET, SOCK_DGRAM, 0);
            if (fd < 0) {
                throw SocketException("socket", true);
            }
            address.sin_family = AF_INET;
            address.sin_addr.s_addr = htonl(INADDR_ANY);
            address.sin_port = htons(port_num);

            if (bind(fd, (struct sockaddr*) &address,
                     (socklen_t) sizeof(address)) < 0) {
                close(fd);
                throw SocketException("bind", true);
            }
        }

        ~UdpSocket() {
            if (fd >= 0) {
                close(fd);
            }
        }

        ssize_t receive(char* buffer, size_t buffer_size, Address& client_address) {
            sockaddr_in client_addr{};
            socklen_t address_length = sizeof(client_addr);
            ssize_t read_length = recvfrom(fd, buffer, buffer_size, 0,
                                           reinterpret_cast<sockaddr*>(&client_addr), &address_length);
            client_address = Address(client_addr);

            if (read_length < 0) {
                if (errno == EWOULDBLOCK) {
                    return 0;
                }
            }

            return read_length;
        }

        void send(char* buffer, size_t buffer_size, Address& client_address) {
            sockaddr_in destination = client_address.get_addr();

            sendto(fd, buffer, buffer_size, 0, (sockaddr*) &destination, sizeof(destination));
        }

        optional<Command> receive_command(Address& client_address, size_t& received_length) {
            received_length = receive(buffer, UDP_PACKET_SIZE, client_address);
            if (received_length == 0) {
                return nullopt;
            }
            return Command::from_bytes(buffer, received_length);
        }

        void send_command(Command& sc, Address& client_address) {
            size_t response_length = sc.to_bytes(buffer);
            send(buffer, response_length, client_address);
        }


        void set_multicast_ttl(int value) {
            if (setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, (void*) &value, sizeof value) < 0) {
                throw SocketException("setsockopt multicast ttl", true);
            }
        }

        void set_broadcast(bool active) {
            int optval = static_cast<int>(active);
            if (setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (void*) &optval, sizeof optval) < 0) {
                throw SocketException("setsockopt broadcast", true);
            }
        }

        void set_multicast_loop(bool allowed) {
            int optval = static_cast<int>(allowed);
            if (setsockopt(fd, SOL_IP, IP_MULTICAST_LOOP, (void*) &optval, sizeof optval) < 0) {
                throw SocketException("setsockopt multicast loop", true);
            }
        }

        void set_multicast_group(string group_address_dotted) {
            ip_mreq ip_mreq{};
            ip_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
            if (inet_aton(group_address_dotted.c_str(), &ip_mreq.imr_multiaddr) == 0) {
                throw SocketException("inet_aton", true);
            }
            if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*) &ip_mreq, sizeof ip_mreq) < 0) {
                throw SocketException("setsockopt multicast", true);
            }
        }

        void set_address_reuse(bool allowed) {
            int optval = static_cast<int>(allowed);
            if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (void*) &optval, sizeof optval) < 0) {
                throw SocketException("setsockopt address reuse", true);
            }
        }

        void set_send_timeout(chrono::system_clock::duration duration) {
            timeval timeout = duration_to_timeval(duration);

            if (setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (char*) &timeout,
                           sizeof(timeout)) < 0)
                throw SocketException("setsockopt send timeout", true);
        }

        void set_receive_timeout(chrono::system_clock::duration duration) {
            timeval timeout = duration_to_timeval(duration);
            if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char*) &timeout,
                           sizeof(timeout)) < 0)
                throw SocketException("setsockopt receive timeout", true);
        }

    };
}

#endif //SIK_NETSTORE2_UDP_SOCKET_HPP
