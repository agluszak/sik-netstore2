#ifndef SIK_NETSTORE2_UTILS_HPP
#define SIK_NETSTORE2_UTILS_HPP

#include <boost/format.hpp>
#include "address.hpp"
#include <iostream>
#include <chrono>
#include <boost/log/trivial.hpp>

namespace netstore2 {

    inline void log_debug(const boost::format& format) {
#ifdef DEBUG
        BOOST_LOG_TRIVIAL(debug) << format.str();
#else
        (void) format;
#endif
    }

    inline void log_error(const boost::format& format) {
#ifdef DEBUG
        BOOST_LOG_TRIVIAL(warning) << format.str();
#else
        clog << format.str() + "\n";
#endif
    }

    inline void log(const boost::format& format) {
#ifdef DEBUG
        BOOST_LOG_TRIVIAL(info) << format.str();
#else
        cout << format.str() + "\n";
#endif
    }

    timeval duration_to_timeval(chrono::system_clock::duration& duration) {
        chrono::microseconds usec = chrono::duration_cast<chrono::microseconds>(duration);
        timeval tv{};
        tv.tv_sec = usec.count() / 1000000;
        tv.tv_usec = usec.count() % 1000000;
        return tv;
    }
}
#endif //SIK_NETSTORE2_UTILS_HPP
