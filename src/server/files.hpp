#ifndef SIK_NETSTORE2_FILES_HPP
#define SIK_NETSTORE2_FILES_HPP

#include <cstdint>
#include <boost/filesystem.hpp>
#include <vector>
#include <string>
#include <algorithm>
#include <exception>
#include <mutex>
#include <shared_mutex>

namespace netstore2 {
    namespace server {
        namespace fs = boost::filesystem;
        using namespace std;

        class Files {
            uint64_t max_space;
            uint64_t free_space;

            fs::path path;
            vector<string> filenames;
            mutable shared_mutex files_mutex;


        public:
            Files(uint64_t max_space, const string& path) :
                    max_space(max_space), path(path) {
                if (!fs::exists(path)) {
                    throw runtime_error("Directory " + path + " does not exist");
                } else if (!fs::is_directory(path)) {
                    throw runtime_error(path + " is not a directory");
                }
                index_files();
            }

            void index_files() {
                unique_lock guard(files_mutex);
                uint64_t occupied = 0;
                filenames.clear();
                for (const fs::directory_entry& entry : fs::directory_iterator(path)) {
                    if (fs::is_regular_file(entry.path())) {
                        occupied += fs::file_size(entry.path());
                        filenames.push_back(entry.path().filename().string());
                    }
                }
                free_space = occupied > max_space ? 0 : max_space - occupied;
            }

            void reserve(uint64_t space) {
                unique_lock guard(files_mutex);
                if (free_space > space) {
                    free_space -= space;
                }
            }

            bool remove(string filename) {
                bool success;
                {
                    unique_lock guard(files_mutex);
                    success = boost::filesystem::remove(path / filename);
                }
                index_files();
                return success;

            }

            uint64_t get_free_space() {
                shared_lock guard(files_mutex);
                return free_space;
            }

            vector<string> get_filenames() {
                shared_lock guard(files_mutex);
                return filenames;
            }

            bool is_present(string filename) {
                shared_lock guard(files_mutex);
                return find(filenames.begin(), filenames.end(), filename) != filenames.end();
            }
        };
    }
}


#endif //SIK_NETSTORE2_FILES_HPP
