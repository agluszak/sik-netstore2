#ifndef SIK_NETSTORE2_SERVER_HPP
#define SIK_NETSTORE2_SERVER_HPP

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "../common/udp_socket.hpp"
#include "../common/types.hpp"
#include "../common/utils.hpp"
#include "files.hpp"
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <algorithm>
#include <boost/algorithm/string/join.hpp>
#include "../common/utils.hpp"
#include <thread>
#include "../common/tcp_server_socket.hpp"

namespace netstore2 {
    namespace server {
        namespace fs = boost::filesystem;

        class Server {
            string multicast_address;
            unsigned short port;
            uint64_t max_space;
            boost::filesystem::path path;
            uint64_t timeout;
            UdpSocket socket;
            Files files;

            void handle_hello(Command& sc, Address& client_address) {
                if (sc.data.length() != 0) {
                    log_error(boost::format(
                            "[PCKG ERROR] Skipping invalid package from %1%. Data was not empty in HELLO") %
                              client_address.to_str());
                }
                Command good_day(CommandName::GOOD_DAY, sc.cmd_seq, files.get_free_space(),
                                 multicast_address);
                socket.send_command(good_day, client_address);
            }

            void handle_list(Command& sc, Address& client_address) {
                vector<string> filenames = files.get_filenames();
                if (sc.data.length() != 0) {
                    filenames.erase(remove_if(filenames.begin(), filenames.end(),
                                              [sc](string& filename) {
                                                  return filename.find(sc.data) == string::npos;
                                              }), filenames.end());
                }

                size_t block_length = 0;
                vector<string> block;
                size_t max_length = 60000;
                for (const string& filename : filenames) {
                    if (filename.length() + 1 > max_length) {
                        throw runtime_error("File name too long - " + filename);
                    }
                    if (block_length + filename.length() + 1 > max_length) {
                        string body = boost::algorithm::join(block, "\n");
                        Command my_list(CommandName::MY_LIST, sc.cmd_seq, body);
                        socket.send_command(my_list, client_address);
                        block_length = 0;
                        block.clear();
                    } else {
                        block.push_back(filename);
                        block_length += filename.length() + 1;
                    }
                }
                if (block_length > 0) {
                    string body = boost::algorithm::join(block, "\n");
                    Command my_list(CommandName::MY_LIST, sc.cmd_seq, body);
                    socket.send_command(my_list, client_address);
                }
            }

            void handle_get(Command& command, Address& client_address) {
                if (!files.is_present(command.data)) {
                    log_error(boost::format("[PCKG ERROR] Skipping invalid package from %1%. No such file to GET") %
                              client_address.to_str());
                } else {
                    try {
                        TcpServerSocket tcp_socket;
                        unsigned short tcp_port = tcp_socket.get_port();
                        log_debug(boost::format("TCP port %1% open for client %2% for downloading") % tcp_port %
                                  client_address.get_address_dot());

                        Command good_day(CommandName::CONNECT_ME, command.cmd_seq, tcp_port,
                                         command.data);
                        socket.send_command(good_day, client_address);

                        thread thread([this, client_address, command, tcp_socket{move(tcp_socket)}]() {
                            try {
                                log_debug(boost::format("Sending file %1%...") % command.data);

                                ifstream file((path / command.data).string(), std::ifstream::binary);
                                TcpConnection connection = tcp_socket.accept(timeout);
                                connection.send_file(file);

                                log_debug(boost::format("File %1% sent!") % command.data);
                            } catch (exception& e) {
                                log_debug(boost::format("Error while sending file %1%: %2%") % command.data % e.what());
                            }
                        });
                        thread.detach();
                    } catch (exception& e) {
                        log_debug(boost::format("Error while sending file %1%: %2%") % command.data % e.what());
                    }
                }
            }

            void send_no_way(Command& command, Address& client_address) {
                Command no_way(CommandName::NO_WAY, command.cmd_seq,
                               command.data);
                socket.send_command(no_way, client_address);
            }


            void handle_add(Command& command, Address& client_address) {
                if (files.is_present(command.data)) {
                    log_error(boost::format(
                            "[PCKG ERROR] Skipping invalid package from %1%. Cannot add file %2% because it's already present") %
                              client_address.to_str() % command.data);
                    send_no_way(command, client_address);
                } else if (command.data.length() == 0) {
                    log_error(boost::format(
                            "[PCKG ERROR] Skipping invalid package from %1%. Cannot add file with empty name") %
                              client_address.to_str());
                    send_no_way(command, client_address);
                } else if (command.data.find('/') != std::string::npos) {
                    log_error(boost::format(
                            "[PCKG ERROR] Skipping invalid package from %1%. Cannot add file %2% with '/' in its name") %
                              client_address.to_str() % command.data);
                    send_no_way(command, client_address);
                } else if (files.get_free_space() < command.param) {
                    log_error(boost::format(
                            "[PCKG ERROR] Skipping invalid package from %1%. Cannot add file %2% because there is not enough free space") %
                              client_address.to_str() % command.data);
                    send_no_way(command, client_address);
                } else {
                    thread thread([this, &client_address, &command]() {
                        try {
                            files.reserve(command.param);
                            TcpServerSocket tcp_socket;
                            unsigned short tcp_port = tcp_socket.get_port();
                            log_debug(boost::format("TCP port %1% open for client %2% for uploading") % tcp_port %
                                      client_address.get_address_dot());

                            Command can_add(CommandName::CAN_ADD, command.cmd_seq, tcp_socket.get_port(), "");
                            socket.send_command(can_add, client_address);
                            log_debug(boost::format("Receiving file %1%...") % command.data);

                            ofstream file((path / command.data).string(), std::ofstream::binary);
                            TcpConnection connection = tcp_socket.accept(timeout);

                            connection.receive_file(file, command.param);
                            log_debug(boost::format("Received file %1%") % command.data);
                        } catch (exception& e) {
                            log_debug(boost::format("Error while sending file %1%: %2%") % command.data % e.what());
                        }
                        files.index_files();
                    });
                    thread.detach();
                }

            }

            void handle_delete(Command& command, Address& client_address) {
                if (command.data.find('/') != string::npos) {
                    log_error(boost::format(
                            "[PCKG ERROR] Skipping invalid package from %1%. Cannot delete file %2% with '/' in its name") %
                              client_address.to_str() % command.data);
                    return;
                }
                try {
                    bool success = files.remove(command.data);
                    if (success) {
                        log_debug(boost::format("Deleted file %1%") % command.data);
                    } else {
                        log_error(boost::format(
                                "[PCKG ERROR] Skipping invalid package from %1%. Cannot delete file %2% because it does not exist") %
                                  client_address.to_str() % command.data);
                    }
                } catch (exception& e) {
                    log_debug(boost::format("Error while deleting file %1%: %2%") % command.data % e.what());
                }

            }

        public:
            Server(
                    const string& multicast_address,
                    unsigned short port, uint64_t
                    max_space,
                    const string& path,
                    uint64_t
                    timeout) :
                    multicast_address(multicast_address), port(port), max_space(max_space), path(path),
                    timeout(timeout),
                    socket(port), files(max_space, path) {

                socket.set_multicast_ttl(4);
                socket.set_multicast_group(multicast_address);
            }

            void start() {

                Address client_address;

                log_debug(boost::format("Server started"));

                while (true) {
                    size_t received_length;
                    optional<Command> sc = socket.receive_command(client_address, received_length);

                    if (sc) {
                        log_debug(boost::format("%1% %2%") % client_address.to_str() % sc->to_str());
                        switch (sc->name) {
                            case CommandName::HELLO:
                                handle_hello(*sc, client_address);
                                break;
                            case CommandName::LIST:
                                handle_list(*sc, client_address);
                                break;
                            case CommandName::GET:
                                handle_get(*sc, client_address);
                                break;
                            case CommandName::ADD:
                                handle_add(*sc, client_address);
                                break;
                            case CommandName::DEL:
                                handle_delete(*sc, client_address);
                                break;
                            default:
                                log_error(boost::format(
                                        "[PCKG ERROR] Skipping invalid package from %1%. Read %2% bytes") %
                                          client_address.to_str() % received_length);
                        }
                    } else {
                        log_error(boost::format("[PCKG ERROR] Skipping invalid package from %1%. Read %2% bytes") %
                                  client_address.to_str() % received_length);
                    }
                }
            }
        };
    }
}

#endif //SIK_NETSTORE2_SERVER_HPP