
#include <boost/program_options.hpp>
#include <iostream>
#include "server.hpp"
#include "../common/utils.hpp"
#include <csignal>


using namespace boost::program_options;
using namespace std;
using namespace std::placeholders;

int main(int argc, const char* argv[]) {

    auto check_range = [](int min, int max, char const* const opt_name) {
        return [opt_name, min, max](unsigned short v) {
            if (v < min || v > max) {
                throw validation_error
                        (validation_error::invalid_option_value,
                         opt_name, std::to_string(v));
            }
        };
    };

    try {
        options_description desc{"Options"};
        desc.add_options()
                ("help,h", "Help screen")
                ("address,g", value<string>()->required(), "MCAST_ADDR adres rozgłaszania ukierunkowanego")
                ("path,f", value<string>()->required(),
                 "SHRD_FLDR ścieżka do dedykowanego folderu dyskowego, gdzie mają być przechowywane pliki")
                ("space,b", value<uint64_t>()->default_value(52428800),
                 "MAX_SPACE maksymalna liczba bajtów udostępnianej przestrzeni dyskowej na pliki grupy przez ten węzeł serwerowy")
                ("timeout,t", value<uint64_t>()->default_value(5)->notifier(check_range(1, 300, "--timeout")),
                 "TIMEOUT (MAX 300) liczba sekund, jakie serwer może maksymalnie oczekiwać na połączenia od klientów")
                ("port,p", value<unsigned short>()->required()->notifier(check_range(0, 65535, "--port")),
                 "CMD_PORT port UDP używany do przesyłania i odbierania poleceń");
        variables_map variables;
        store(parse_command_line(argc, argv, desc), variables);

        if (variables.count("help")) {
            cout << desc << endl;
            return 0;
        }

        notify(variables);

        netstore2::log_debug(boost::format("Server starting"));
        netstore2::server::Server server(variables["address"].as<string>(), variables["port"].as<unsigned short>(),
                                         variables["space"].as<uint64_t>(), variables["path"].as<string>(),
                                         variables["timeout"].as<uint64_t>());

        signal(SIGPIPE, SIG_IGN);
        server.start();

    } catch (const exception& ex) {
        cerr << "[ERROR] " << ex.what() << endl;
    }

    return 0;

}